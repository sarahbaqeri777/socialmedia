from fastapi import Depends,HTTPException,APIRouter,status
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
import schemas,models,utils,oauth2
from database import get_db,SessionLocal


router=APIRouter(tags=["authentication"])

@router.post("/login",response_model=schemas.Token)
def user_login(user_credentials : OAuth2PasswordRequestForm=Depends(),db:Session=Depends(get_db)):
    user = db.query(models.User).filter(models.User.email== user_credentials.username).first()
    
    if not user :
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,detail="incorrect credential")
    
    if not utils.verify(user_credentials.password,user.password):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,detail="incorrect credentials")
    
    access_token=oauth2.create_access_token(data={"user_id":user.id})
    
    return access_token
