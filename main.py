from multiprocessing import synchronize
from fastapi import FastAPI,Depends,status
from pydantic import BaseModel
from db import get_db,engine
import models
import routers.post
import routers.auth
import routers.user


models.Base.metadata.create_all(bind=engine)
app= FastAPI()


app.include_router(routers.post.router)
app.include_router(routers.user.router)
app.include_router(routers.auth.router)


@app.get('/')
async def root():
    return {'massage':'welcome to my application'}

