from fastapi import FastAPI,Depends,status,HTTPException,APIRouter
from sqlalchemy.orm import Session
from typing import List,Optional
import schemas,models,utils,database,oauth2
from database import get_db
from sqlalchemy import func

router = APIRouter(
    prefix="/posts",
    tags=["post"]
)

@router.post('/posts',status_code=status.HTTP_201_CREATED,response_model=schemas.PostOut)
def create_post(post:schemas.CreatePost,db:Session=Depends(get_db),current_user:int=Depends(oauth2.get_current_user)):
    new_post=models.Post(owner_id=current_user.id,**post.dict())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)
    return new_post

@router.get("/posts",response_model=List[schemas.PostOut])
def get_all_posts(db:Session=Depends(get_db),current_user:int=Depends(oauth2.get_current_user),limit:int=3,skip:int=0,search:Optional[str]=""):
    post_list=db.query(models.Post,func.count(models.Vote.post_id).label("votes")).filter(models.Post.title.contains(search)).limit(limit).offset(skip).all()
    
    result = db.query(models.Post).join(models.Vote,models.Vote.post_id==models.Post.id,isouter=True).group_by(models.Vote.post_id).filter(models.Post.title.contains(search)).limit(limit).offset(skip).all()
    
    return post_list


@router.get("/posts/{id}",response_model=schemas.PostOut)
def get_post(id:int,db:Session=Depends(get_db),current_user:int=Depends(oauth2.get_current_user)):
        post=db.query(models.Post).filter(models.Post.owner_id==id).first()

        if not post:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="not found")
        return post

@router.put("/posts/{id}",response_model=schemas.PostOut)
def update_post(id:int,updated_post:schemas.CreatePost,db:Session=Depends(get_db),current_user:int=Depends(oauth2.get_current_user)):
    post = db.query(models.Post).filter(models.Post.id==id)
    post2 = post.first()
    if post2 == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="post not found")
    
    if post.owner_id != oauth2.current_user.id :
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,detail="not authorized")
    
    post.update(updated_post.dict(),synchronize_session=False)
    
    db.commit()
    return post.first()
 

@router.delete("/posts/{id}",status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id:int,db:Session=Depends(get_db),current_users:int=Depends(oauth2.get_current_user)):
    
    post_query=db.query(models.Post).filter(models.Post.id==id)

    post=post_query.first()
    
    if post == None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="not found")
        
    if post.owner_id != oauth2.current_users.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,detail="not authorized")

    
    post_query.delete(synchronize_session=False)
    db.commit()