from fastapi import Depends,HTTPException,APIRouter,status
from sqlalchemy.orm import Session
from .. import schemas,models,utils
from db import get_db,SessionLocal


router=APIRouter(tags=["authentication"])

@router.post("/login")
def user_login(user_credentials : schemas.UserLogin,db:Session=Depends(get_db)):
    user = db.query(models.User).filter(models.User.email== user_credentials.email).first()
    
    if not user :
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="incorrect credential")
    
    if not utils.verify(user_credentials.password,user.password):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="incorrect credentials")
    
    return {"token":"tokennnn"}
