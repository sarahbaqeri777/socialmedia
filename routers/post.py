from fastapi import FastAPI,Depends,status,HTTPException,APIRouter
from sqlalchemy.orm import Session
from db import get_db
from typing import List,Optional
import schemas,models

router = APIRouter(
    prefix="/posts",
    tags=["post"]
)

@router.post('/posts',status_code=status.HTTP_201_CREATED,response_model=schemas.PostOut)
def create_post(post:schemas.CreatePost,db:Session=Depends(get_db)):
    new_post=models.Post(**post.dict())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)
    return new_post

@router.get("/posts",response_model=List[schemas.PostOut])
def get_all_posts(db:Session=Depends(get_db)):
    post_list=db.query(models.Post).all()
    return post_list


@router.get("/posts/{id}",response_model=schemas.PostOut)
def get_post(id:int,db:Session=Depends(get_db)):
        post=db.query(models.Post).filter(models.Post.id==id).first()

        if not post:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="not found")
        return post

@router.put("/posts/{id}",response_model=schemas.PostOut)
def update_post(id:int,updated_post:schemas.CreatePost,db:Session=Depends(get_db)):
     post = db.query(models.Post).filter(models.Post.id==id)
     post2 = post.first()
     if post2 == None:
         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="post not found")
    
  
     post.update(updated_post.dict(),synchronize_session=False)
    
     db.commit()
     return post.first()
 

@router.delete("/posts/{id}",status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id:int,db:Session=Depends(get_db)):
    
    post=db.query(models.Post).filter(models.Post.id==id)

    if post.first() == None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="not found")

    
    post.delete(synchronize_session=False)
    db.commit()