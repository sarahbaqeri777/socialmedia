from pydantic import BaseModel,EmailStr
from datetime import datetime


class BasePost(BaseModel):
    title:str
    content :str
    published :bool=True
    
class CreatePost(BasePost):
    pass 

class UpdatePost(BasePost):
    pass 

class PostOut(BasePost):
    id : int
    created_at : datetime
    
    class Config:
        orm_mode = True

class UserCreate(BaseModel):
    email:EmailStr
    password:str
    
class UserOut(BaseModel):
    id : int
    email : EmailStr
    created_at : datetime
    
    class Config:
        orm_mode = True
        
class UserLogin(BaseModel):
    email : EmailStr
    password : str