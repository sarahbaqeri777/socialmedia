from fastapi import FastAPI,Depends,status,HTTPException,APIRouter
from app.database import SessionLocal, get_db
import database,models,schemas,oauth2
from sqlalchemy.orm import Session



router = APIRouter(
    prefix="/vote",
    tags=["Vote"],
)

@router.post("/",status_code=status.HTTP_201_CREATED)
def vote(vote:schemas.Vote,db:Session=Depends(get_db),current_user:int=Depends(oauth2.get_current_user)):
    
    post = db.query(models.Post).filter(models.Post.id==vote.post_id).first()
    post_query = db.query(models.Vote).filter(models.Vote.post_id == vote.post_id, models.Vote.user_id == current_user.id)
    post_found= post_query.first()
    if not post :
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="post does not exist")
    if vote.dir == 1 :
        if post_found:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,detail="this user has voted before")
        
        new_vote = models.Vote(post_id = vote.post_id, user_id = current_user.id)
        db.add(new_vote)
        db.commit()
        
    else:
        if not post_found:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="the vote does not exist")
        
        post_query.delete(synchronize_session=False)
        db.commit()
        
        return {"message": "successfully deleted vote"}
            